<?php

require_once "img-helper.php";

?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../css/gallery1.css" />
</head>
<body>
<div class="main-container">
      <div class="codrops-top clearfix">
        <a class="codrops-icon codrops-icon-prev" href="gallery.php"><span>Go back to gallery</span></a>
      </div>
<div class="container">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
  
  <?php 
        $i = 0;  
        foreach($images as $image){
            if($i == 0){
        ?>
        <div class="carousel-item active">
        <img src="<?php echo $image; ?>" class="d-block w-100" alt="..." ?>
        </div>
        <?php               
        $i++;   } else{
            if($i != 0){    
        ?>
        <div class="carousel-item">
        <img src="<?php echo $image; ?>" class="d-block w-100" alt="..." ?>
        </div>
            <?php                           
            } $i++;
        }  
          }
        ?>
    
    <!-- <div class="carousel-item">
      <img src="img/4.jpg" class="d-block w-100" alt="...">
    </div>-->
  </div> 
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
