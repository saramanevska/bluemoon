<?php
require_once "img-helper.php";
// shuffle($images);

//pagination 
$page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
$total = count( $images ); //total items in array    
$limit = 18; //per page    
$totalPages = ceil( $total/ $limit ); //calculate total pages
$page = max($page, 1); //get 1 page when $_GET['page'] <= 0
$page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
$offset = ($page - 1) * $limit;
if( $offset < 0 ) $offset = 0;

$images = array_slice( $images, $offset, $limit );
?>
<!DOCTYPE html>
<html>
<head>
	<title>Blue Moon</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/style1.css">
	<link rel="stylesheet" type="text/css" href=" ../css/menu1.css">
	<link rel="stylesheet" type="text/css" href="../css/gallery-main1.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>

	<div class="menu-wrap">
		<input type="checkbox" class="toggler">
		<div class="hamburger"><div></div></div>
		<div class="menu">
			<div>
				<div>
					<ul>
						<li><a href="../index.html">Home</a></li>
						<li><a href="gallery.php">Gallery</a></li>
						<li><a href="../about_me.html">About</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="content">
		<h1>Gallery</h1>
	</div>
	
	<div class="gallery-wrapper">
	<a  href="sliderbot.php">
		
		<?php
			for($i=0; $i < count($images); $i++)
			echo "<img src=\"$images[$i]\">";
		?>
		
	</a>
	</div>
    <div class="page_current">
		<?php for ($counter = 1; $counter <= $totalPages; $counter++) { ?>
			<a id="paginationLink" href="gallery.php?page=<?php echo $counter; ?>&perPage=<?php echo $limit;?>"><?php echo $counter; ?></a>	
        <?php } ?>
	</div>
	<footer>
		<div id="footer-container">
			<div id="contact-me">
			<h3>Contact me</h3>
			<p><img src="envelope-regular.png">monikaeye@gmail.com</p>
			</div>
			<div id="icons">
				<a href="#"><img src="facebook-square-brands.png"></a>
				<a href="#"><img src="instagram-brands.png"></a>
				<a href="#"><img src="etsy-brands.png"></a>
			</div>
			<div id="location">
			<h3>Location</h3>
			<p>Skopje, North Macedonia</p>
			</div>
		</div>
		<p id="copyright">All rights are reserved. &copy Monika Ilievska 2020</p>	
    </footer>	
</body>
</html>