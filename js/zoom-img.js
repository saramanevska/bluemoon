document.getElementById('zoom').addEventListener('mousemove', function (event) {

    let xPos = event.offsetX/ this.offsetWidth*100;
    let yPos = event.offsetY/ this.offsetHeight*100;
    this.style.backgroundPosition = xPos+'% '+yPos+'%';
    this.style.backgroundSize = "350%";
})

document.getElementById('zoom').addEventListener('mouseout', function(event){
    this.style.backgroundSize = "100%";
    })
